/*
Copyright 2023 Google LLC

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/robedwards/buildey/cli/cmd/document"
	"gitlab.com/robedwards/buildey/cli/cmd/info"
	"gitlab.com/robedwards/buildey/cli/cmd/review"
	codechat "gitlab.com/robedwards/buildey/cli/vertex"
)

var Version = "0.0.5"

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:     "buildey",
	Version: Version,
	Short:   "A GenAI assistent for usage in CI workflows",
	Long:    ``,
	Run: func(cmd *cobra.Command, args []string) {
		promptFlag, _ := cmd.Flags().GetString("prompt")

		if promptFlag != "" {

			fmt.Println("Going to pass the following as a GenAI prompt: ", promptFlag)
			fmt.Println(codechat.Chat(promptFlag))

		} else {

			// fmt.Println("standard no flag")
			cmd.Help()
		}
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Whoops. There was an error while executing your CLI '%s'", err)
		os.Exit(1)
	}
}

func init() {
	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.AddCommand(document.DocumentCmd)
	rootCmd.AddCommand(info.InfoCmd)
	rootCmd.AddCommand(review.ReviewCmd)

	// rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.buildey.yaml)")
	rootCmd.PersistentFlags().StringP("prompt", "p", "", "Addiontal text to pass to the model prompt, if within options it will append to existing prompts while if at the root of the command it will be the only prompt passed.")

	// releaseCmd.Flags().StringP("file", "f", "", "The file to use for this action")
	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
