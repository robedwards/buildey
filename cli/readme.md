# Command Line

An iteration on the Python scripts, refactored to be written in golang which will mak eit easier to create an executable and provided an oppertunity to learn the language.



```
go build -o bin/buildey
```

## Build the container

To build the container image clone this repo and then run the following command to build the image. It assumes that you have the artifact registry and cloud build API enabled and configured. It will create a images with the tag `europe-docker.pkg.dev/$PROJECT_ID/buildey-image/buildey'`

```
gcloud builds submit --region europe-west2 --config ./cloudbuild.yaml .
```



To increment version during build process;

https://www.thorsten-hans.com/lets-build-a-cli-in-go-with-cobra/
```
go build -o ./bin/buildey -ldflags="-X 'gitlab.com/robedwards/buildey/cli/cmd/buildey.version=0.0.2'" main.go
```



# TODO

* [ ] Add to Buildey Builer image to be used by Cloud Build
* [ ] Pipeline to build and complie the CLI into releases
* [ ] Implement auto version incramation on builds
* [ ] Update the demo example to use the golang CLI rather than the Python scripts
* [ ] Tidy up and streamline the builder image
* [ ] Code Review checklist
* [ ] Break down the CLI into more reusable chunks of code (at the moment quite inefficient)