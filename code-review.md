# Caffeine Boost Your Code Reviews with AI

Can GenAI assisted development tools help with the code reveiw process?

Imagine a world where your coding gets an extra jolt of creativity – like a double-shot espresso for your development process! That's where DuetAI comes in. It's your AI sidekick, ready to brew up fresh lines of code, explain those tricky bits, and troubleshoot problems faster than you can say 'mocha latte.'

We've all been there – that distracting jump out of the IDE to find a solution, followed by the inevitable black hole of online distractions. It's a buzzkill for your focus! With DuetAI, you stay in the zone, saving time and keeping your coding flow as smooth as a velvety latte. After all, studies show that every distraction steals a whopping 34 minutes of your time.

And let's talk code review. It's like a quality check for your code, and it deserves top-notch attention.  A good code review is a caffeine boost for everyone involved, fueling better code and sharper skills. We know from the awesome research from DORA how important code reviews are for software delivery performance [(Teams with faster code reviews have
50% higher higher software delivery performance)](https://cloud.google.com/devops/state-of-devops).

Ah, the world of code reviews – where lines of code are scrutinized with the intensity of a detective solving a mystery! But guess what, folks? We're about to embark on a thrilling journey where AI (Artificial Intelligence) steps into the code review arena. Buckle up because we're about to explore this exciting, informative, fun, and engaging topic in style!


The typical steps involved in code reviews are;
* CREATE - Arthur writes, modifies or removes code in the code base. Once ready prepares the change to be submitted (PR, MR, CL etc.)
* PREVIEW - Author looks at the diff and self reviews (1st pass). Automated tooling could support this (static analysis, code analysers). Once happy add reviewer (or team)
* REVIEW - Reviewer(s) look at the submission considering things like maintainability, readability, quality, correctness, increase own knowledge of code base. Discuss and provides feedback.
* APPROVE/REJECT - Based on incorporation of any feedback …… “lgtm”


## Within the IDE

These are the inner loop aspects leveraging Cloud Code and DuetAI. We know that DuetAI is capable of helping with code generation, completion, explanation but can it help review?

What do we do in the "preview" stage? We want to ensure the code is of high quality and make life easier for the person about to review! That can consist of;
* Self Review (inc. automated tools, linters and `diff` view)
* Clean up code, useful comments and documentation
* Ensure easy to understand and organised
* Useful commit messages
* Has an updated tests (although possibly wrong way around if TDD)
* Code compiles and passes tests 

NOTE: use you own code or leverage some of the example code in `./example_code` like [`coffee.go`](./example_code/coffee.go).

To a quick review of what has changed and do a self-code review. The git plugin in Code OSS can be useful for this
  ![](img/Screenshot%20from%202023-12-08%2011-58-07.png)

Some tips;
*  Commit the specific code that has been changed for this new feature or functionality. `git add -p` is a method I like to use to ensure only the changes I wanted are added to the commit.
*  Add a relevant comment to the commit and also check that comments have been added as required

Questions to ask DuetAI chat in the IDE (these can vary depending on the language)
* Does this code have meaningful Doc Comments?
   * Fix this for me
* Does this code handle errors correctly?
* Does this code follow Go standards?
* Does this code pass values it should?
* Does this code pass maintainability, readability and quality checks?

![](./img/manual-questions.gif)

Typing each of these can be time consuming, inconsitend and fustrating! Can the questions be stored in a git repo to provide a team or org level approach ..... yes it can.

Look at `qu.txt` in the IDE, you can see a number of questions (note the wording and style to for prompt engineering). Ask DuetAI `What are the questions in qu.txt` and then ask it to `Could you do a code review with the questions in qu.txt on coffee.go?`.

![](./img/qu-txt-file.gif)

Although what if you are new to a team or do not know where to start (I think I watched the film inception too much)? What not ask DuetAI what it would ask and then aks it to compare the code to those questions;
* `As a professional code reviewer who understands what good code should look like and how to optimze provide a list of the top 5 questions you would ask about this specific code ? The questions should be able to be answered with yes or no.`
* `Now review the code with these questions answering yes or no. When you answer no provide some context to why.`

![](./img/inception.gif)

Maybe you could just striaght out ask `How would you improve this code?`.

![](./img/how-would-you.gif)

When you are happy its time to submit to the approval process, whatever that may be.


## Continuous Integration workflows

So can we use GenAI to help the reviewer? Note: While I talk about CI workflows here it might make more sense to have this integrated into pre-commits.

Can we have some automation to do the heavy lifting and get a quick snapshot of common question that AI is good at providing feedback freeing up the reviewer to focus on the more vale add aspects of a code review. Thinks that are best suited for humans?

In steps a [Cloud Build Builder image](https://cloud.google.com/build/docs/cloud-builders) that has a CLI using the [Codey API](https://cloud.google.com/vertex-ai/docs/generative-ai/code/code-models-overview).

The CLI is build using golang and the [Cobra framework](https://cobra.dev/). Explanation of this to follow.

![](./img/buildy-code-review.gif)

An example output after Cloud build triggers a workflow is;

![](./img/cloud-build-workflow.png)


# Summary

Can AI replace code reviews? Well lets 1st answer the question of why we do code reviews ...

3 benefits of code review;
* Knowledge transfer
* Integrity of codebase
* Ensure readability and consistency

While more exist these are the common themes, as we can see AI cannot and should not replace the human reviewer however it can help with the heavy lifting allowing humans to focus on the biggest value adds of review and also help transfer knowledge.