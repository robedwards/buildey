# Cloud Build Builder Image

This is a basic cloud builder image that incorporates the VertexAI Codey API. It is used to demo Codey in the CI process when showcasing DuetAI.

The CLI has been migrated from [Python](./python/) to [golang](./golang/) to simplify an number of aspects. Going forward the python version will be deprecated and should be used for a reference on how to used the codey SDK in Python. 

# Roadmap

* new options based on feedback and customer conversations 
* more flexibility passing in code to be processed by Codey
* experiment with other chat models
* code optimisation (buildey.py), its very much rapid MVP

Some flags to consider
`buildey document release -g "github.com... -f <file location> -o <output location>` 

Read a pre-created file or clone git repo and do the diff
Print to screen or -o to output to a location


## Demos

* [Code Review Demo](./code-review.md)


